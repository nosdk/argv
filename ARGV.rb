module NoSDK
  module ARGV
    module_function

    # All parsed argument based on configuration
    # @return [Hash]
    def all
      return @result if @config == (config = NoSDK.component_conf['ARGV'])

      @config = config
      arguments = ::ARGV.dup

      return show_help if arguments.include?('--help') || arguments.include?('-h')

      @result = result = {}
      parse_positional_arguments(arguments, result)
      parse_child_arguments(arguments, result)
      parse_kwarguments(arguments, result)
      parse_flags(arguments, result)

      return result
    end

    # @param arguments [Array<String>]
    # @param result [Hash]
    def parse_positional_arguments(arguments, result)
      @config[:positional_argument_list].each do |name|
        return show_help("Missing argument #{name}") if arguments.empty? || arguments.first.start_with?('-')

        result[name] = arguments.shift
      end

      @config[:optional_argument_list].each do |name|
        break if arguments.first.start_with?('-')

        result[name] = arguments.shift
      end

      if @config[:has_infinite_arguments]
        result[:args] = []
        result[:args] << arguments.shift until arguments.first.start_with?('-')
      end
    end

    # @param arguments [Array<String>]
    # @param result [Hash]
    def parse_kwarguments(arguments, result)
      result = (result[:kwargments] = {})
      @config[:kwargments].each do |name, info|
        real_name = info[:is_single_dash] ? "-#{name}" : "--#{name}"
        current_arg = arguments.find { |arg| arg.start_with?(real_name) }
        unless current_arg
          show_help("Missing argument #{real_name}") if info[:required]
          result[name] = info[:value_if_absent]
          next
        end
        if current_arg != real_name
          result[name] = current_arg.sub(real_name, '')
          result[name] = result[name][1..-1] unless info[:is_single_dash] 
        else
          index = arguments.index(current_arg) + 1
          result[name] = arguments[index] if arguments[index]
          arguments.delete_at(index)
        end
        arguments.delete(current_arg)

        result[name] = info[:cast_proc].call(result[name]) if info[:cast_proc]
      end
    end

    # @param arguments [Array<String>]
    # @param result [Hash]
    def parse_flags(arguments, result)
      result = (result[:flags] = {})
      @config[:flags].each do |name, info|
        real_name = info[:is_single_dash] ? "-.*#{name}.*" : "--#{name}"
        if arguments.any? { |arg| arg.match?(real_name) }
          result[name] = info[:value_if_set]
        else
          result[name] = info[:value_otherwise]
        end
      end
    end

    class << self
      private

      # @param arguments [Array<String>]
      # @param result [Hash]
      def parse_child_arguments(arguments, result)
        child_index = arguments.index('--')
        if child_index
          result[:child_args] = arguments[(child_index + 1)..-1]
          result[:child_args_string] = result[:child_args].map { |string| string.include?(' ') ? string.dump : string }.join(' ')
          arguments.keep_if.with_index { |_, index| index < child_index }
        end
      end

      def show_help(msg = nil)
        puts "Error: #{msg}" if msg

        puts @config[:application_name].to_s.center(80, '=')
        puts @config[:application_description]
        puts '=' * 80

        puts "#{$0} #{positional_help} [...flags/kwargs] [-- child_args]"
        puts flag_help
        puts kwarg_help
        Process.exit!(msg ? 1 : 0)
      end

      def positional_help
        pos = @config[:positional_argument_list].join(' ')
        opt = @config[:optional_argument_list].join('] [')
        return pos if opt.empty?

        return "#{pos} [#{opt}]"
      end

      def flag_help
        flags = @config[:flags].map do |name, info|
          real_name = info[:is_single_dash] ? "-#{name}" : "--#{name}"
          description = info[:description] || 'No description'
          next "  #{real_name} : #{description}"
        end
        return ['Flags'.center(80, '=')].concat(flags)
      end

      def kwarg_help
        kargs = @config[:kwargments].map do |name, info|
          real_name = info[:is_single_dash] ? "-#{name}" : "--#{name}"
          description = info[:description] || 'No description'
          required = info[:required] ? '(required) ' : nil
          required ||= info[:value_if_absent] ? "(default: #{info[:value_if_absent]}) " : nil
          next "  #{real_name} #{required}: #{description}"
        end
        return ['Named arguments'.center(80, '=')].concat(kargs)
      end
    end
  end
end
